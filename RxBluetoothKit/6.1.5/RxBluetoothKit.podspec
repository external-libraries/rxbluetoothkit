Pod::Spec.new do |s|
  s.name             = "RxBluetoothKit"
  s.version          = "6.1.5"
  s.summary          = "Bluetooth library for RxSwift"
  s.author           = { 'Bragi' => 'mobile.apps@bragi.com' }

  s.description      = <<-DESC
  RxBluetoothKit is lightweight and easy to use Rx support for CoreBluetooth.
                       DESC

  s.homepage         = "https://gitlab.com/external-libraries/rxbluetoothkit"
  s.source           = { :git => "https://gitlab.com/external-libraries/rxbluetoothkit", :tag => s.version }

  s.ios.deployment_target = '14.0'
  #s.osx.deployment_target = '10.13'
  # s.watchos.deployment_target = '4.0'
  # s.tvos.deployment_target = '11.0'
  s.swift_version = '5.0'

  s.requires_arc = true

  s.source_files = 'Source/*.swift'
  s.osx.exclude_files = 'Source/RestoredState.swift', 'Source/CentralManager+RestoredState.swift', 'Source/CentralManagerRestoredState.swift'
  s.frameworks   = 'CoreBluetooth'
  s.dependency 'RxSwift', '~> 6.8.0'
end
